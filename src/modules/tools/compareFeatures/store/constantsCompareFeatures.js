const
    iconEmptyStar = "<span class=\"glyphicon glyphicon-star-empty\" style=\"font-size:22px;\"></span>",
    iconYellowStar = "<span class=\"glyphicon glyphicon-star\" style=\"color:#fec44f; font-size:22px;\"></span>";

export {
    iconEmptyStar,
    iconYellowStar
};
